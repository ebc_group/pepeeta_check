const config = require("./config").getConfig();
const MongoClient = require('mongodb').MongoClient;
// MongoClient.connect(config.MONGO_URL, function (err, client) {
//     if (err) throw err;
//     console.log("db created!");
//     const db = client.db(config.DB_NAME);
//     client.close();
// });

/**
 * OSS: INUTILIZZATO
 * @param buyer 
 */
function insertBuyer(buyer) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, { useNewUrlParser: true }, (err, client) => {
            if (err)
                reject(err);
            else {
                const db = client.db(config.DB_NAME);
                let entry = {
                    wallet: buyer.wallet,
                    stepNumber: buyer.stepNumber,
                    amount: buyer.amount,
                    pep: buyer.pep
                }
                db.collection("buyers").insertOne(
                    entry,
                    ((err, res) => {
                        if (err) reject(err);
                        else {
                            console.log("salvataggio effettuato");
                            client.close();
                            resolve(true);
                        }
                    })
                );
            }
        });
    });
}

/**
 * Carica dalla collection buyers tutti gli acquirenti che hanno 
 * il campo step number uguale a quello passato come parametro
 * @param {number} stepNumber 
 */
function retrieveBuyers(stepNumber) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, { useNewUrlParser: true }, (err, client) => {
            if (err) return reject(err);

            const db = client.db(config.DB_NAME);
            db.collection("buyers").distinct("buyer", { "stepNumber": stepNumber },
                ((error, docs) => {
                    // il metodo toArray ritorna un array di array
                    if (error) reject(error);
                    else resolve(docs);
                    // la connessione va chiusa sia in caso di riuscita che in caso 
                    // di errore
                    client.close();
                })
            );
        });
    });
}

/**
 * Invoca i metodi per aggiornare gli utenti a cui viene inviata la fattura.
 * In base al parametro stepNumber invoca il giusto metodo per fare la scrittura sul db
 * @param {any[]} customers è un array contenente 
 * oggetti del tipo {wallet:<address_string>, balanceStepX: <number>}
 * @param {Number} stepNumber
 * @returns {Promise[]} un array di Promise contenenti i risultati degli update sul db
 */
function updateCustmers(customers, stepNumber) {
    let promises = [];
    MongoClient.connect(config.MONGO_URL, { useNewUrlParser: true }, (err, client) => {
        if (err) throw err;
        else {

            console.log("AGGIORNAMENTO DATABASE");
            console.log("buyers da aggiornare: " + JSON.stringify(customers));
            const db = client.db(config.DB_NAME);
            switch (stepNumber) {
                case 1:
                    // nello step 1 va fatto un insert, nei successivi step un update
                    customers.forEach(customer => {
                        promises.push(insertSingleBuyerStep1(customer, db));
                    });
                    break;
                case 2:
                    customers.forEach(customer => {
                        promises.push(insertSingleBuyer(customer, db, 2));
                    });
                    break;
                case 3:
                    customers.forEach(customer => {
                        promises.push(insertSingleBuyer(customer, db, 3));
                    });
                    break;
                default:
                    console.log("ERROR durante la query");
                    // throw (new ERROR("ERRORE durante la query"));
                    reject(new Error("ERROR durante la query"));
            }
            client.close();
            // return promises;
        }
    });
    return promises;
}

/**
 * Inserisce sul db gli utenti a cui viene spedita la fattura
 * dello step 1
 * @param {{wallet: String, info: Number}} customer
 * @param {*} db 
 * @returns una Promise contenente i risultati della scrittura
 */
function insertSingleBuyerStep1(customer, db) {
    return new Promise((resolve, reject) => {
        let query = { buyer: customer.wallet, stepNumber: 1, balanceStep1: customer.info };
        console.log("[QUERY] " + JSON.stringify(query));
        db.collection("buyers").insertOne(query, (err, res) => {
            if (err) reject(err)
            else resolve(res);
        });
    });
}

/**
 * Inserisce sul db gli utenti a cui viene spedita la fattura
 * di uno step.
 * @param {{wallet: String, info: Number}} customer
 * @param {*} db la connessione al db
 * @returns una Promise contenente i risultati della scrittura
 */
function insertSingleBuyer(customer, db, stepNumber) {
    return new Promise((resolve, reject) => {
        let query = { buyer: customer.wallet, "stepNumber": stepNumber, amount: customer.info };
        console.log("[QUERY] " + JSON.stringify(query));
        db.collection("buyers").insertOne(query, (err, res) => {
            if (err) reject(err)
            else resolve(res);
        });
    });
}

/**
 * Aggiorna gli utenti a cui è appena stata inviata la fattura per lo step 2
 * @param {{wallet: String, info: Number}} customer è l'oggetto che va aggiornato sul db
 * @param {*} db la connessione al database 
 */
function updateSingleBuyerStep2(customer, db) {
    return new Promise((resolve, reject) => {
        let queryFilter = { "buyer": customer.wallet };
        let queryUpdate = { "$set": { "stepNumber": 2, "balanceStep2": customer.info } };
        console.log("[QUERY] " + JSON.stringify(queryFilter) + ", " + JSON.stringify(queryUpdate));
        db.collection("buyers").updateOne(queryFilter, queryUpdate, (err, res) => {
            if (err) reject(err);
            else resolve(res);
        });
    });
}

/**
 * Aggiorna gli utenti a cui è appena stata inviata la fattura per lo step 3
 * @param {{wallet: String, info: Number}} customer è l'oggetto che va aggiornato sul db
 * @param {*} db la connessione al database
 * @returns una Promise contenente i risultati della scrittura sul database
 */
function updateSingleBuyerStep3(customer, db) {
    return new Promise((resolve, reject) => {
        let queryFilter = { "buyer": customer.wallet };
        let queryUpdate = { "$set": { "stepNumber": 3, "balanceStep3": customer.info } };
        console.log("[QUERY] " + JSON.stringify(queryFilter) + ", " + JSON.stringify(queryUpdate));
        db.collection("buyers").updateOne(queryFilter, queryUpdate, function (err, res) {
            if (err) reject(err);
            else resolve(res);
        });
    });
}

exports.insertBuyer = insertBuyer;
exports.retrieveBuyers = retrieveBuyers;
exports.updateCustmers = updateCustmers;
