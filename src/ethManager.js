var Web3 = require("web3");
var config = require("./config").getConfig();
var utility = require("./utility");
var BigNumber = require('bignumber.js');
var web3 = getWeb3();
/**
 * Funziona che crea l'istanza di web3
 */
function getWeb3() {
    // let web3;
    if (typeof web3 !== 'undefined') {
        // console.log("[web3] gia definito");
        web3 = new Web3(web3.currentProvider);
    } else {
        // Set the provider you want from Web3.providers
        // console.log("[web3] nuova istanza");
        web3 = new Web3(new Web3.providers.HttpProvider(config.WEB3_CONNECTION_ADDRESS));
    }
    return web3;
}

function unlockAccount() {
    console.log(web3.personal.unlockAccount(config.MAIN_ADDRESS, config.PASS_MAIN_ADDR, 600));
}

var pepeetaContractInstance = web3.eth.contract(config.ABI_PEPEETA).at(config.PEPEETA_CONTRACT);
var icoContractInstance = web3.eth.contract(config.ABI_ICO).at(config.ICO_CONTRACT);

/**
 * verifica se il cap1 è stato raggiunto
 * @returns una Promise contentente un booleano: true se il cap e'
 * stato raggiunto, false altrimenti
 */
function checkCap1Reached() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep1Reached((error, result) => {
            if (error) reject(error);
            else {
                resolve(result);
            }
        });
    });
}

function checkCap2Reached() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep2Reached((error, result) => {
            if (error) reject(error);
            else {
                resolve(result);
            }
        });
    });
}

function checkCap3Reached() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep3Reached((error, result) => {
            if (error) reject(error);
            else {
                resolve(result);
            }
        });
    });
}

function getCapStep1() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep1((error, result) => {
            if (error) reject(error);
            else
                resolve(result.toNumber());
        });
    });
}

function getCapStep2() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep2((error, result) => {
            if (error) reject(error);
            else
                resolve(result.toNumber());
        });
    });
}

function getCapStep3() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.capStep3((error, result) => {
            if (error) reject(error);
            else
                resolve(result.toNumber());
        });
    });
}

/**
 * Ritorna dalla blockchain le informazioni relative all'indirizzo inserito
 * come parametro
 * 
 * @param address l'indirizzo del wallet
 * @returns Una promise contenente un array cosi fatto:
 * result[0]: bool, privateEnabled
 * reuslt[1]: bool, standardEnabled
 * result[2]: BigNumber, balanceStep1
 * result[3]: BigNumber, balanceStep2
 * result[4]: BigNumber, balanceStep3 
 */
function getWallet2user(address) {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.wallet2user(address, (error, result) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

/**
 * @returns una Promise contenete l'array con gli indirizzi
 * di coloro che hanno fatto un acquisto privato
 */
function getPrivateBuyers() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.getPrivateBuyers((error, result) => {
            if (error) reject(error);
            else {
                resolve(result);
            }
        });
    });
}

function getStandardBuyers() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.getStandardBuyers((error, result) => {
            if (error) reject(error);
            else {
                resolve(result);
            }
        });
    });
}

/**
 * Interroga lo smart contract per conoscere l'attuale step
 * @returns una Promise contenente il numero dello step
 */
function getCurrentStep() {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.currentStep((error, result) => {
            if (error) reject(error);
            else resolve(result.toNumber())
        })
    })
}

/**
 * ritorna dalla blockchain tutti gli acquirenti
 * @returns Una Promise
 */
function getAllBuyersFromBC() {
    return new Promise((resolve, reject) => {
        Promise.all([
            getPrivateBuyers(),
            getStandardBuyers()
        ])
            .then(arrayRes => {
                let buyers = utility.eliminaDoppioni(arrayRes[0].concat(arrayRes[1]));
                resolve(buyers);
            })
            .catch(reason => reject(reason));
    });
}

/**
 * Invoca il metodo wallet2user dello smart contract e ritorna un array di array
 * contenenti le informazioni sugli acquisti degli utenti
 * @param {String[]} addressArray sono gli indirizzi dei quali prelevare informazioni
 * @returns {[[boolean, boolean, number, number, number, number, number number],[number]]}
 * una Promise contenente array di array, dove gli array sono cosi fatti: 
 * @property isPrivateEnabled
 * @property isStandardEnabled
 * @property balanceStep1 (in wei)
 * @property balanceStep2 (in wei)
 * @property balanceStep3 (in wei)
 */
function getInfoUsers(addressesArray, stepNumber) {
    return new Promise(async (resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        let promisesInfo = [];
        let promisesPrivatePurchases = []
        addressesArray.forEach(address => {
            promisesInfo.push(getWallet2user(address));
            promisesPrivatePurchases.push(getPrivatePurchasesSingleUser(address, stepNumber));
        });
        // Promise.all(promisesInfo.concat(promisesPrivatePurchases))
        Promise.all(promisesInfo)
            .then(res => {
                Promise.all(promisesPrivatePurchases)
                    .then(packages => {
                        // res è un array di array, dove ogni arrai contiene le info dell'utente.
                        console.log("INFO UTENTI" + JSON.stringify([res, packages]));
                        resolve([res, packages]);
                    })

            })
            .catch(reason => {
                console.error("errore in promises.all dentro getInfoUsers");
                console.error(reason);
                reject(reason);
            });
    });
}

/**
 * 
 * @param {String} address wallet dell'utente
 * @param {Number} stepNumber step number dell'acquisto richiesto
 * @returns {Promise<number>} Una promise contenente il pacchetto acquistato
 */
function getPrivatePurchasesSingleUser(address, stepNumber) {
    return new Promise((resolve, reject) => {
        if (!web3.isConnected()) {
            reject(new Error("Web3 is not connected"));
            return;
        }
        icoContractInstance.getUserPrivatePurchases(
            address, stepNumber, (error, result) => {
                if (error) reject(error);
                else resolve(result.toNumber());
            });
    });
}

/**
 * converte wei in ether
 * @param {string|number|BigNumber} wei valore da convertire
 * @returns {number}
 */
function weiToEther(wei) {
    return Number(web3.fromWei(wei, "ether"));
}




exports.unlockAccount = unlockAccount;
exports.checkCap1Reached = checkCap1Reached;
exports.getCapStep1 = getCapStep1;
exports.checkCap2Reached = checkCap2Reached;
exports.getCapStep2 = getCapStep2;
exports.checkCap3Reached = checkCap3Reached;
exports.getCapStep3 = getCapStep3;
exports.getPrivateBuyers = getPrivateBuyers;
exports.getStandardBuyers = getStandardBuyers;
exports.getCurrentStep = getCurrentStep;
exports.getAllBuyersFromBC = getAllBuyersFromBC;
exports.getInfoUsers = getInfoUsers;
exports.getPrivatePurchasesSingleUser = getPrivatePurchasesSingleUser;
exports.weiToEther = weiToEther;