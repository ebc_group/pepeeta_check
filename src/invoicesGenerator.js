const PDFDocument = require('pdfkit');
const fs = require("fs");

/**
 * 
 * @param {String} wallet 
 * @param {Number} etherStandardPurchase 
 * @param {Number} etherPrivatePurchase 
 * @param {Number} pepStandardPurchase
 * @param {Number} pepPrivatePurchase
 * @param {Number} stepNumber
 */
function generate(
    wallet,
    etherStandardPurchase,
    etherPrivatePurchase,
    pepStandardPurchase,
    pepPrivatePurchase,
    stepNumber
) {
    // Create a document
    let letter_size = { size: [595, 842] }
    let doc = new PDFDocument(letter_size);
    let path = "./invoices"
    let filename = wallet.toString() + "_step_" + stepNumber.toString() + ".pdf";
    doc.pipe(fs.createWriteStream("./invoices/" + filename));

    // CONFIGURAZIONI
    doc.info.Author = "Giacomo Onori";
    doc.info.Title = "Invoice";

    //INIZIO DISEGNO
    let origin_x = doc.x;

    // LOGO
    doc.image('./images/Logo_colore.png', origin_x, doc.y,
        {
            width: 200
        }
    );

    let date = new Date();
    let options = {
        weekday: "short",
        year: "numeric",
        month: "long",
        timeZone: "UTC"
    };
    let data_str = date.toLocaleDateString("en-GB", options);
    doc.fontSize(19)
        .text("INVOICE", 125, 150, { align: "right" })
        .fontSize(11)
        .moveDown(2)
        .text("DATE: " + data_str, { align: "right" });

    doc.x = origin_x; // ripristino la x originale (72pt)
    doc.moveDown(3)
        .text("Bill to:", doc.x, doc.y)
        .text("wallet " + wallet.toString(), doc.x, doc.y);

    // DISEGNO TABELLA
    // riga intestazione
    doc.moveDown(1)
        .roundedRect(doc.x, doc.y, 595 - (doc.x * 2), 20, 5)
        .fillAndStroke("#DCDCDC", "black");

    doc.fillColor("black")
        .text("Item", doc.x + 15, doc.y + 5)
        .moveUp(1)
        .text("Ether", doc.x + 150, doc.y)
        .moveUp(1)
        .text("PEP", doc.x + 150, doc.y);

    let ascissaColonne = origin_x;
    let ordinataColonne = doc.y + 1;
    let ordinataColonneFinale = ordinataColonne + 200;

    // prima linea verticale
    doc.moveTo(ascissaColonne, ordinataColonne)
        .lineTo(ascissaColonne, ordinataColonneFinale)
        .stroke();

    // seconda linea verticale
    let offsetAscissa = 150;
    doc.moveTo(ascissaColonne + offsetAscissa, ordinataColonne)
        .lineTo(ascissaColonne + offsetAscissa, ordinataColonneFinale)
        .stroke();

    // terza linea verticale
    // offsetAscissa += 150;
    doc.moveTo(ascissaColonne + (offsetAscissa * 2), ordinataColonne)
        .lineTo(ascissaColonne + (offsetAscissa * 2), ordinataColonneFinale)
        .stroke();


    // quarta linea verticale
    doc.moveTo(595 - ascissaColonne, ordinataColonne)
        .lineTo(595 - ascissaColonne, ordinataColonneFinale)
        .stroke();

    // linea orizzontale fine tebella
    doc.moveTo(ascissaColonne, ordinataColonneFinale)
        .lineTo(595 - ascissaColonne, ordinataColonneFinale)
        .stroke();
    // FINE DISEGNO TABELLA

    // INSERIMENTO ITEMS 
    doc.fontSize(10);
    if (etherStandardPurchase !== 0) {
        ordinataColonne += 10;
        doc.text("Token(s) standard purchase ", ascissaColonne, ordinataColonne + 1)
            .text(etherStandardPurchase, ascissaColonne + offsetAscissa, ordinataColonne + 1)
            .text(pepStandardPurchase.toString(), ascissaColonne + (offsetAscissa * 2), ordinataColonne, { align: "right" });
        ordinataColonne = doc.y;
    }

    if (etherPrivatePurchase !== 0) {
        ordinataColonne += 10;
        doc.text("Token(s) private purchase ", ascissaColonne, ordinataColonne + 1)
            .text(etherPrivatePurchase, ascissaColonne + offsetAscissa, ordinataColonne + 1)
            .text(pepPrivatePurchase.toString(), ascissaColonne + (offsetAscissa * 2), ordinataColonne, { align: "right" });
        ordinataColonne = doc.y;
    }

    // FONDO PAGINA
    let _width = 150;  // grandezza desiderata del logo
    let ascissaLogoFooter = 595 - 72 - _width
    doc.image('./images/Logo_lettering_colore.png', ascissaLogoFooter, 700,
        {
            width: _width
        }
    );

    // Finalize PDF file*/
    doc.end();
}

exports.generate = generate;