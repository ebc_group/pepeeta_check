const step1 = require("./steps/step1");
const step2 = require("./steps/step2");
const step3 = require("./steps/step3");
const readline = require('readline-sync');

function start() {
    switch (stepNumber) {
        case 1:
            console.log("CASO 1");
            step1.avvioCaso1();
            break;
        case 2:
            // devo inviare le ricevute dello step 1
            // faccio il merge delle due liste; prendo dallo SC i dati
            // relativi ad ogni utente (in particolare il campo balanceStep1);
            // genero le fatture; faccio un insert con stepNumer = 1
            console.log("CASO 2");
            step2.avvioCaso2();
            break;
        case 3:
            //devo inviare le ricevute dello step 2.
            console.log("CASO 3");
            step3.avvioCaso3();
            break;
        default:
            console.log("OPSS... IL VALORE INSERITO NON CORRISPONDE A NESSUN CASO");

    }
}



// numero dello step da inviare le fatture
var message = "\n\n**********\ndriiiiinn, driiiiiinn... Benvenuto nel POSTINO!!!\n********\n\n\n";
var stepNumber;
console.log(message);
setTimeout(function() {
    stepNumber = Number.parseInt(readline.question("step number? "));
    start(); // AVVIO DEL PROGRAMMA
}, 2000);
