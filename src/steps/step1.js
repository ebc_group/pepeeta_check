const ethManager = require("../ethManager");
const mongodb = require("../dbManager");
const utility = require("../utility");
const bignumber = require("bignumber.js");
const invoiceGenerator = require("../invoicesGenerator");


/**
 * esegue tutte le operazioni per lo step 1
 */
function avvioCaso1() {
    Promise.all([
        mongodb.retrieveBuyers(1),
        ethManager.getPrivateBuyers(),
        ethManager.getStandardBuyers(),
        ethManager.checkCap1Reached()
    ])
        .then(arrayRes => {
            let giaFatturati = arrayRes[0]; // sono gli acquirenti a cui ho gia spedito la fattura
            let allBuyers = utility.eliminaDoppioni(arrayRes[1].concat(arrayRes[2])); // sono gli acquirenti presi dalla BC e puliti dei doppioni
            if (!arrayRes[3]) throw (new Error("Il cap 1 non è stato raggiunto!!"));
            // arrayDiff sono gli account a cui devo inviare la fattura
            let addressesDaFatturare = utility.arrayDiff(allBuyers, giaFatturati);
            // ora con ogni elemento dell'arrayDiff devo prelevare i dati dalla BC,
            // inviare la fattura e salvare il tutto sul DB
            ethManager.getInfoUsers(addressesDaFatturare, 1) // 1 è lo stepNumber
                .then((arrayInfoUsers) => {
                    // qui devo inviare la fattura e salvare nel db.
                    let custmersToUpdate = creaFatture(addressesDaFatturare, arrayInfoUsers[0], arrayInfoUsers[1]);
                    // SALVATAGGIO SUL DB
                    console.log("FATTURE GENERATE!!");
                    Promise.all(mongodb.updateCustmers(custmersToUpdate, 1))
                        .then((arrayPromises) => console.log("DATABASE AGGIORNATO PER LO STEP 1"))
                        .catch(reason => console.error(reason))
                })
                .catch(reason => console.error(reason));
        })
        .catch(reason => console.error(reason));
}

/**
 * Genera le fatture e crea un pdf
 * @param {String[]} addresses contiene gli indirizzi da fatturare
 * @param {[boolean, boolean, number, number, number, number, number, number]} arrayInfoUsers 
 * contiene le informazioni degli utenti da fatturare
 * @param {[number]} arrayPackagesBuyed è l'id del pacchetto acquistato
 */
function creaFatture(addresses, arrayInfoUsers, arrayPackagesBuyed) {
    let i = 0;
    let custmersToUpdate = [];
    arrayInfoUsers.forEach(user => {
        console.log("elemento: " + JSON.stringify(user));
        // al metodo generate l'indirizzo, gli ether per l'acquisto standard, gli ether 
        // per l'acquisto privato, i pep standard, i pep privati
        if (user[2].toNumber() !== 0) {
            console.log("** CREAZIONE FATTURE **");
            const pepPrivatePurchase = utility.getPepFromPackageNumber(arrayPackagesBuyed[i]);
            const weiPrivatePurchase = utility.getWeiFromPackage(arrayPackagesBuyed[i]);
            const weiStandardPurchase = user[2].toNumber() - weiPrivatePurchase;
            const pepStandardPurchase = user[5] - pepPrivatePurchase;
            // invoiceGenerator.generate(addresses[i], user[2].toNumber(), pepPrivatePurchase, 1);
            invoiceGenerator.generate(addresses[i], ethManager.weiToEther(weiStandardPurchase),
                ethManager.weiToEther(weiPrivatePurchase), pepStandardPurchase, pepPrivatePurchase, 1);
            custmersToUpdate.push({ wallet: addresses[i], info: user[2].toNumber() });
        }
        i += 1;
    });
    console.log("custmersToUpdate: " + JSON.stringify(custmersToUpdate));
    return custmersToUpdate;
}

exports.avvioCaso1 = avvioCaso1;