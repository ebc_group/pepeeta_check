const ethManager = require("../ethManager");
const mongodb = require("../dbManager");
const utility = require("../utility");
const bignumber = require("bignumber.js");
const invoiceGenerator = require("../invoicesGenerator");

function avvioCaso2() {
    Promise.all([
        mongodb.retrieveBuyers(2),
        ethManager.getPrivateBuyers(),
        ethManager.getStandardBuyers(),
        ethManager.checkCap2Reached()
    ])
        .then((arrayRes) => {
            let giaFatturati = arrayRes[0]; // sono gli acquirenti a cui ho gia spedito la fattura
            let allBuyers = utility.eliminaDoppioni(arrayRes[1].concat(arrayRes[2])); // sono gli acquirenti presi dalla BC e puliti dei doppioni
            if (!arrayRes[3]) throw (new Error("Il cap 2 non è stato raggiunto!!"));

            let addressesDaFatturare = utility.arrayDiff(allBuyers, giaFatturati);
            ethManager.getInfoUsers(addressesDaFatturare, 2)
                .then(arrayInfoUsers => {
                    let custmersToUpdate = creaFatture(addressesDaFatturare, arrayInfoUsers[0], arrayInfoUsers[1]);
                    // SALVATAGGIO SUL DB
                    Promise.all(mongodb.updateCustmers(custmersToUpdate, 2))
                        .then((arrayPromises) => console.log("DATABASE AGGIORNATO PER LO STEP 2"))
                        .catch(reason => console.error(reason));
                })
                .catch(reason => console.error(reason));
        })
        .catch(reason => console.error(reason));
}

/**
 * Genera le fatture e crea un pdf
 * @param {String[]} addresses contiene gli indirizzi da fatturare
 * @param {[boolean, boolean, number, number, number, number, number, number]} arrayInfoUsers 
 * contiene le informazioni degli utenti da fatturare
 */
function creaFatture(addresses, arrayInfoUsers, arrayPackagesBuyed) {
    let i = 0;
    let custmersToUpdate = [];
    arrayInfoUsers.forEach(user => {
        if (user[3].toNumber() !== 0) {
            console.log("** CREAZIONE FATTURE **");
            const pepPrivatePurchase = utility.getPepFromPackageNumber(arrayPackagesBuyed[i]);
            const weiPrivatePurchase = utility.getWeiFromPackage(arrayPackagesBuyed[i]);
            const weiStandardPurchase = user[3].toNumber() - weiPrivatePurchase;
            const pepStandardPurchase = user[6] - pepPrivatePurchase;
            invoiceGenerator.generate(addresses[i], ethManager.weiToEther(weiStandardPurchase),
                ethManager.weiToEther(weiPrivatePurchase), pepStandardPurchase, pepPrivatePurchase, 2);
            custmersToUpdate.push({wallet: addresses[i], info: user[3].toNumber()});
        }
        i += 1;
    });
    console.log("custmersToUpdate: "+JSON.stringify(custmersToUpdate));
    return custmersToUpdate;
}

exports.avvioCaso2 = avvioCaso2;