const ethManager = require("../ethManager");
const mongodb = require("../dbManager");
const utility = require("../utility");
const bignumber = require("bignumber.js");
const invoiceGenerator = require("../invoicesGenerator");

function avvioCaso3() {
    Promise.all([
        mongodb.retrieveBuyers(3),
        ethManager.getPrivateBuyers(),
        ethManager.getStandardBuyers(),
        ethManager.checkCap3Reached()
    ])
        .then((arrayRes) => {
            let giaFatturati = arrayRes[0]; // sono gli acquirenti a cui ho gia spedito la fattura
            let allBuyers = utility.eliminaDoppioni(arrayRes[1].concat(arrayRes[2])); // sono gli acquirenti presi dalla BC e puliti dei doppioni
            if (!arrayRes[3]) throw (new Error("Il cap 3 non è stato raggiunto!!"));

            console.log("gia fatturati " + giaFatturati);
            let addressesDaFatturare = utility.arrayDiff(allBuyers, giaFatturati);
            ethManager.getInfoUsers(addressesDaFatturare, 3)
                .then(arrayInfoUsers => {
                    let custmersToUpdate = creaFatture(addressesDaFatturare, arrayInfoUsers[0], arrayInfoUsers[1]);
                    // SALVATAGGIO SUL DB
                    Promise.all(mongodb.updateCustmers(custmersToUpdate, 3))
                        .then((arrayPromises) => console.log("DATABASE AGGIORNATO PER LO STEP 3"))
                        .catch(reason => console.error(reason));
                })
                .catch(reason => console.error(reason));
        })
        .catch(reason => console.error(reason));
}

/**
 * Genera le fatture e crea un pdf
 * @param {String[]} addresses contiene gli indirizzi da fatturare
 * @param {[boolean, boolean, number, number, number, number, number, number]} arrayInfoUsers 
 * contiene le informazioni degli utenti da fatturare
 */
function creaFatture(addresses, arrayInfoUsers, arrayPackagesBuyed) {
    let i = 0;
    let custmersToUpdate = [];
    arrayInfoUsers.forEach(user => {
        if (user[4].toNumber() !== 0) {
            console.log("** CREAZIONE FATTURE **");
            const pepPrivatePurchase = utility.getPepFromPackageNumber(arrayPackagesBuyed[i]);
            const weiPrivatePurchase = utility.getWeiFromPackage(arrayPackagesBuyed[i]);
            const weiStandardPurchase = user[4].toNumber() - weiPrivatePurchase;
            const pepStandardPurchase = user[7] - pepPrivatePurchase;
            invoiceGenerator.generate(addresses[i], ethManager.weiToEther(weiStandardPurchase),
                ethManager.weiToEther(weiPrivatePurchase), pepStandardPurchase, pepPrivatePurchase, 3);
            custmersToUpdate.push({wallet: addresses[i], info: user[4].toNumber()});
        }
        i += 1;
    });
    console.log("custmersToUpdate: "+JSON.stringify(custmersToUpdate));
    return custmersToUpdate;
}

exports.avvioCaso3 = avvioCaso3;