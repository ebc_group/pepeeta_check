function eliminaDoppioni(array) {
    let output = [];
    array.forEach(element => {
        if (output.indexOf(element) == -1)
            output.push(element);
    });
    return output;
}

function getBuyerAddressesFromArray(array, property) {
    let output = [];
    array.forEach(element => {
        output.push(element[property]);
    });
    return output
}

/**
 * Assume che arr1 sia privo di doppioni 
 * @param arr1 
 * @param arr2 
 * @returns Un array contenente il merge dei due
 */
function mergeArrays(arr1, arr2) {
    let output = [];
    arr1.concat(output);
    output = output.concat(arr1);
    arr2.forEach(element => {
        if (output.indexOf(element) == -1)
            output.push(element);
    });
    return output;
}

/**
 *  
 * @param completo 
 * @param parziale 
 * @returns Un array contenente gli elementi che sono presenti nell'array completo
 * ma non sono presenti nell'array parziale
 */
function arrayDiff(completo, parziale) {
    let diff = [];
    completo.forEach(element => {
        if (parziale.indexOf(element) == -1)
            diff.push(element);
    });
    return diff;
}

/**
 * prende il numero di PEP corrispondenti all'id del pacchetto passato
 * come parametro
 * @param {number} packageNumber id del pacchetto
 * @returns {number} Il numero di pep corrispondente al package in input
 */
function getPepFromPackageNumber(packageNumber) {
    switch (packageNumber) {
        case 1:
            return 100;
        case 2:
            return 300;
        case 3:
            return 500;
        default:
            return 0;
        // throw new Error("Package number sconosciuto");
    }
}

function getWeiFromPackage(packageNumber) {
    switch (packageNumber) {
        case 1:
            return 1000000000000000000;
        case 2:
            return 2000000000000000000;
        case 3:
            return 3000000000000000000;
        default:
            return 0;
    }
}

exports.eliminaDoppioni = eliminaDoppioni;
exports.getBuyerAddressesFromArray = getBuyerAddressesFromArray;
exports.mergeArrays = mergeArrays;
exports.arrayDiff = arrayDiff;
exports.getPepFromPackageNumber = getPepFromPackageNumber;
exports.getWeiFromPackage = getWeiFromPackage;